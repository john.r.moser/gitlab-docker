upstream gitlab {
	server gitlab.upstream.com;
}

server {
	listen unix:/var/run/nginx/gitlab.socket default_server;

	root /var/www/apps/gitlab;
	index index.html index.htm;

	server_name git.example.com;

	access_log /dev/stdout;
	error_log /dev/stderr;

	location / {
		proxy_pass http://gitlab/;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Host $host;
	}

	location ~ /\.ht {
		deny all;
	}
}

upstream kanban {
	server kanban.upstream.com;
}

server {
	listen unix:/var/run/nginx/git-kanban.socket;

	server_name kanban.git.example.com;

	access_log /dev/stdout;
	error_log /dev/stderr;

	location / {
		proxy_pass http://kanban;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Host $host;
	}

	location /ws {
		proxy_pass http://kanban;
		proxy_set_header Host $host;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		proxy_set_header Origin "";
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	}

	location ~ /\.ht {
		deny all;
	}
}

