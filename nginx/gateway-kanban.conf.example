# Upstream git-kanban
upstream git-kanban {
	server unix:/var/run/nginx/git-kanban.socket;
}

# send to https
server {
	listen 80;
	listen [::]:80;
	server_name kanban.git.example.com;
	return 301 https://$server_name$request_uri;
}

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;
	ssl on;
	ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
	ssl_session_cache shared:SSLdefault:1m;

	# HSTS
	proxy_hide_header Strict-Transport-Security;
	add_header Strict-Transport-Security "max-age=31536000" always;

	server_name kanban.git.example.com;

	client_max_body_size 1M;

	location /.well-known/acme-challenge/ {
		root /var/www/html;
	}

	location / {
		proxy_pass http://git-kanban;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Host $server_name;
	}

	location /ws {
		proxy_pass http://git-kanban;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Host $server_name;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		proxy_set_header Origin "";
	}
}


